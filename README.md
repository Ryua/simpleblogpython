# simpleBlogPython

Simple Python script for creating a blog from markdown posts.

![](images/indexLight.png)
![](images/indexDark.png)
![](images/entryLight.png)
![](images/entryDark.png)

## Dependencies

- Python3
- Pandoc

## TODO

- Iterative entry creation

## Markdown posts

Markdown posts must have a yaml header of this type on the first 3 lines.
The md file must have the the title with the form entry+id. (Ex: entry1.md)

---
{"entry": 1, "title": "post 1", "desc": "desc1", "date": "01 Jan 2022"}
---

## License
WTFPL
