import os
import json 
import subprocess
import shutil

#always use absolute paths
postsPath = "/home/user/posts/folder"
outputPath = "/home/user/output/folder"

blogURL = "http://localhost:8000"
blogName = "Blog Test"
email = "teste@blog.com"
rssDesc = "Updates for Blog Test"
rssLang = "pt-pt"

#flag to clean or not the out folder 
#if iterative flag is true this flag will not matter
cleanOutFolder = True 

#if false this script will generate all the site structure and files
#if true it will only add the missing entries after the last one
isIterative = False   

globalVars = {
  'blogName':blogName, 
  'blogURL': blogURL,
  'email': email,
  'rssDesc': rssDesc,
  'rssLang': rssLang
}

jsCode = """
window.onload=function() 
{
  togg = document.getElementById("toggleThemeBut"); 
  body = document.getElementsByTagName('body')[0];
  if(document.cookie == "") {
    document.cookie = "light; path=/";
  }
  else if(document.cookie == "dark") {
    togg.innerHTML = "light";
    body.classList.remove("lightTheme");
    body.classList.add("darkTheme");
  }
};
function ToggleTheme() {
  if(togg.innerHTML == "dark" && body.classList.contains("lightTheme")) {
    togg.innerHTML = "light";
    body.classList.remove("lightTheme");
    body.classList.add("darkTheme");
    document.cookie = "dark; path=/";
  } 
  else if(togg.innerHTML == "light" && body.classList.contains("darkTheme")) {
    togg.innerHTML = "dark";
    body.classList.remove("darkTheme");
    body.classList.add("lightTheme");
    document.cookie = "light; path=/";
  }
}
"""

cssRules = """
body {
    background-color: var(--color-primary);
    color: var(--font-color);
    font-family: 'EB Garamond';
    font-style: normal;
    font-size: 18px;
}
body a {
  color: var(--font-color);
}
.darkTheme {
    --color-primary: #121212;
    --color-secondary: #243133;
    --color-accent: #12cdea;
    --font-color: #ffffff;
}
.lightTheme {
    --color-primary: #ffff;
    --color-secondary: #fbfbfe;
    --color-accent: #fd6f53;
    --font-color: #000000;
}
.container {
    display: flex;
    justify-content: center;
}
.main { 
  width: 700px;
  margin-left: 5%;
  margin-right: 5%;
  margin-top: 40px;
  line-height: 30px;
}
.banner {
  display: flex;
  font-family: 'EB Garamond';
  font-style: normal;
  justify-content: center;
}
.banner .links a {
  padding-right: 15px;
}
.posts {
  list-style-type: none;
}
.posts a{
  padding-left: 10px;
  text-decoration: none;
}
.posts li {
  color: gray;
}
footer .contact{
  display: flex;
  justify-content: center;
}
footer a{
  display: block;
  margin-bottom: 20px;
  margin-top: 15px;
  text-decoration: none;
}
footer hr {
  width: 40%;
}
"""

htmlTemp = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>%(blogName)s</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=EB+Garamond:400,500" rel="stylesheet" type="text/css">
  <link rel="icon" href="data:;base64,=">
  <link rel="stylesheet" href="https://gitcdn.link/cdn/necolas/normalize.css/master/normalize.css">
  <link rel="stylesheet" href="[css]">
  <script src="[js]"></script> 
</head>
<body class="lightTheme">
  <div class="banner">
    <div class="bannerWrapper">
      <h1>%(blogName)s</h1>
      <div class="links">
        <a href="%(blogURL)s">home</a>
        <a href="%(blogURL)s/rss.xml">rss</a>
        <a href="#" id="toggleThemeBut" onclick="ToggleTheme();return false;">dark</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="main">
        [content]
      <div>
        <a style="float:right;" href="#top">top</a>
      </div>
    </div>
  </div>
    <footer>
        <hr>
        <div class="contact">
            <a href="mailto:%(email)s">%(email)s</a>
        </div>
    </footer>
</body>
</html>
"""

def get_entry(entry):
    return entry.get('entry')

def get_title(entry):
    return entry.get('title')

def get_date(entry):
    return entry.get('date')

def get_desc(entry):
    return entry.get('desc')

def getPostsHeaders():
  postFiles = os.listdir(postsPath)
  headers = []
  for item in postFiles:
      with open(os.path.join(postsPath, item), 'r') as f:
          for position, line in enumerate(f):
              if position == 1:
                  headers.append(json.loads(line))
  return headers

def createIndexList(listDict):
    postsList = """<ul class="posts">"""
    listDict.sort(key=get_entry)
    listDict.reverse()
    for item in listDict:
        postsList = postsList + "\n<li> " + get_date(item) + "<a href=\"" + "/posts/""" + "entry" + str(get_entry(item)) + ".html\">" + get_title(item) + "</a> </li>"
    postsList = postsList + "\n</ul>"
    return postsList

def createPageContent(isIndex = True, post = None):
  html = htmlTemp%globalVars
  if isIndex:
    indexList = createIndexList(getPostsHeaders())
    html = html.replace("[css]", "css/custom.css")
    html = html.replace("[js]", "js/custom.js")
    html = html.replace("[content]", indexList)
    return html
  elif not isIndex and post != None:
    html = html.replace("[css]", "../css/custom.css")
    html = html.replace("[js]", "../js/custom.js")
    html = html.replace("[content]", post)
    return html


def createRssBody():
  rssTemplate = """<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>%(blogName)s</title>
    <description>%(rssDesc)s</description>
    <language>%(rssLang)s</language>
    <link>%(blogURL)s</link>
    <atom:link href="%(blogURL)s/rss.xml" rel="self" type="application/rss+xml"/>"""
  rssTemplate = rssTemplate%globalVars
  return rssTemplate

def createRssEntry(entry):
  rssEntryTemplate = """    <item>
      <title>%(title)s</title>
      <guid>[link]</guid>
      <link>[link]</link>
      <pubDate>%(date)s</pubDate>
      <description>%(desc)s</description>
    </item>"""
  rssEntryTemplate = rssEntryTemplate%entry
  link = blogURL + "/posts/entry" + str(get_entry(entry)) + ".html"
  rssEntryTemplate = rssEntryTemplate.replace("[link]", link)
  return rssEntryTemplate

def getLastEntry():
  lastEntryLine = linecache.getline(outputPath+"/index.html", 30)
  lastEntry = lastEntryLine.split('"')[1]
  return int(lastEntry[12:-5])

def getMissingEntries(lastEntry):
  headers = getPostsHeaders()
  headers.sort(key=get_entry)
  return headers[lastEntry:]


#cleans out folder
if cleanOutFolder:
  shutil.rmtree(outputPath)
  os.mkdir(outputPath)

#create paths 
cssPath = os.path.join(outputPath, "css")
jsPath = os.path.join(outputPath, "js")
entryPath = os.path.join(outputPath, "posts")
#create folders
os.mkdir(cssPath)
os.mkdir(jsPath)
os.mkdir(entryPath)

#create css
os.chdir(cssPath)
f = open("custom.css", "x") 
f.write(cssRules)
f.close()

#create js
os.chdir(jsPath)
f = open("custom.js", "x") 
f.write(jsCode)
f.close()

#create index
html = createPageContent()
os.chdir(outputPath)
f = open("index.html", "x") 
f.write(html)
f.close()

#create rss
rss = createRssBody()
headers = getPostsHeaders()
headers.sort(key=get_entry)
for header in headers:
  rss = rss + "\n" + createRssEntry(header)
rss = rss + "\n  </channel>\n</rss>"
f = open("rss.xml", "x") 
f.write(rss)
f.close()

#create entries
os.chdir(os.path.join(outputPath, "posts"))
postFiles = os.listdir(postsPath)
for fileName in postFiles:
  path = os.path.join(postsPath, fileName)
  command = "pandoc -f markdown -t html " + path
  entry = subprocess.check_output(command, shell=True)
  htmlEntry = createPageContent(False, entry.decode("utf-8")) 
  f = open(fileName[:-3] + ".html", "x") 
  f.write(htmlEntry)
  f.close()

print("Blog created")
